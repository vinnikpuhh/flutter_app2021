import 'dart:async';
import 'package:alt_point_food/models/food.dart';
import 'package:flutter/foundation.dart';

class DBMock with ChangeNotifier {
  List<Food> _foods = List<Food>();
  int _itemCount = 0;
  bool _isLoaded = false;

  DBMock() {
    buildFoods();
  }

  void buildFoods() async {
    await Future.delayed(Duration(seconds: 3));
    _foods.add(Food(
        name: "Дерзкая Марго",
        desc: "Настолько же дерзкая, насколько Марго",
        numOfReviews: 176,
        ETA: "20-30 мин",
        price: 1300,
        rating: 4.5,
        pic: 'https://192.168.1.1:44302/Pizza.png'
    ));
    _foods.add(Food(
        name: "Горячий парень",
        desc: "Главное, не заказывай это блюдо на работе перед коллегами",
        numOfReviews: 176,
        ETA: "25-30 мин",
        price: 300,
        rating: 4.5,
        pic: 'https://192.168.1.1:44302/HotBoy.png'
    ));
    _foods.add(Food(
        name: "Вкусняшка Миа",
        desc: "Wanna taste her?",
        numOfReviews: 176,
        ETA: "20-30 мин",
        price: 300,
        rating: 4.5,
        pic: 'https://192.168.1.1:44302/Mia.png'
    ));
    _foods.add(Food(
        name: "Сытый Питт",
        desc: "И, судя по всему, боди-позитивный",
        numOfReviews: 176,
        ETA: "20-30 мин",
        price: 300,
        rating: 4.5,
        pic: 'https://192.168.1.1:44302/Burger.png'
    ));
    _itemCount = _foods.length;
    _isLoaded = true;
    notifyListeners();
  }

  void update() async {
    //TODO
  }

  List<Food> get foods => _foods;

  int get size => _itemCount;

  bool get ready => _isLoaded;
}
