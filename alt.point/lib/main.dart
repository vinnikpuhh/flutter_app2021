import 'package:alt_point_food/screens/MainScreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dbmock/DBMock.dart';
import 'models/cart.dart';

void main() {
  runApp(Main());
}

class Main extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => Cart(),
      child: MaterialApp(
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: RemoveGlowBehaviour(),
            child: child
          );
        },
        debugShowCheckedModeBanner: false,
        title: 'Alt.Point Food',
        theme: ThemeData(
          primaryColor: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
          accentColor: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: ChangeNotifierProvider(
          create: (_) => DBMock(),
          child: MainScreen()
        ),
      ),
    );
  }
}

class RemoveGlowBehaviour extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
