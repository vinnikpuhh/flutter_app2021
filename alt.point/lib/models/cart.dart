import 'package:alt_point_food/models/food.dart';
import 'package:flutter/foundation.dart';

class Cart extends ChangeNotifier {

  List<Food> _foods = new List<Food>();
  int count = 0;

  Cart();

  void add(Food food) {
    _foods.add(food);
    update();
  }

  void remove(Food food) {
    _foods.remove(food);
    update();
  }

  update() {
    count = _foods.length;
    notifyListeners();
  }

  double get totalPrice {
    double total = 0;
    _foods.map((e) => total += e.price);
    return total;
  }
}