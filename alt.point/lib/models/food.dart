class Food {

  String name;
  String desc;
  int price;
  String pic;
  double rating;
  int numOfReviews;
  String ETA;

  Food({this.name, this.desc, this.price, this.pic, this.rating, this.numOfReviews, this.ETA});
}