import 'package:alt_point_food/dbmock/DBMock.dart';
import 'package:alt_point_food/screens/widgets/CustomDrawer.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  @override
  Widget build(BuildContext context) {
    var db = context.watch<DBMock>();
    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
      drawer: CustomDrawer(),
      appBar: AppBar(
        elevation: 0,
        leading: Builder(
          builder: (context) => GestureDetector(
            child: Padding(
              padding: EdgeInsets.only(left: 40),
              child: Icon(Icons.menu),
            ),
            onTap: () {Scaffold.of(context).openDrawer();},
          ),
        ),
        title: Padding(
          padding: EdgeInsets.only(left: 20),
          child: Row(
            children: [
              Text('Челябинск', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400)),
              SizedBox(width: 16),
              SvgPicture.asset('assets/down.svg')
            ],
          ),
        ),
        actions: <Widget>[
          Padding(padding: EdgeInsets.only(right: 40), child: CartButton())
        ],
      ),
      body: CustomScrollView(
        slivers: [
          Collapsable(),
          Tabs(),
          db.ready ? SliverList(delegate: SliverChildBuilderDelegate((context, index) => MenuItem(db.foods[index]), childCount: db.foods.length)) :
              SliverList(delegate: SliverChildListDelegate([Center(child: CircularProgressIndicator())]))
        ],
      ),
    );
  }
}
