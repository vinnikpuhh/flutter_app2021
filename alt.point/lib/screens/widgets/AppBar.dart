import 'package:alt_point_food/models/cart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/svg.dart';

class CartButton extends StatelessWidget { //BUG AFTER 100 ITEMS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  @override
  Widget build(BuildContext context) {
    String val = context.watch<Cart>().count.toString();
    return Container(
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          SvgPicture.asset('assets/shoppingCart.svg'),
          ClipOval(
            child: Container(
              child: SizedBox(
                width: 16,
                height: 16,
                child: Center(
                  child: Text(val, style: TextStyle(fontSize: 10), overflow: TextOverflow.visible)
                )
              ),
              color: Color.fromARGB(0xff, 0xf2, 0xc9, 0x4c),
            ),
          )
        ],
        alignment: AlignmentDirectional.bottomStart,
      ),
    );
  }
}
