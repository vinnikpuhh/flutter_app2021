import 'package:alt_point_food/models/cart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      child: Row(
        children: [
          ClipOval(
            child: SizedBox(
              height: 45,
              width: 45,
              child: Image.asset('assets/ava.png'),
            )
          ),
          Container(
            padding: EdgeInsets.only(left: 15),
            width: 150,
            child: Column(
              children: [
                Text(
                  'Juanita Nguyen',
                  style: TextStyle(fontWeight: FontWeight.w500),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'somemail@mail.com',
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Color.fromARGB(0xff, 0x82, 0x82, 0x82)
                  ),
                  overflow: TextOverflow.ellipsis,
                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          ),
          GestureDetector(onTap: () {Navigator.pop(context);},child: Padding(padding: EdgeInsets.only(left: 5), child: SvgPicture.asset('assets/out.svg')))
        ],
        mainAxisAlignment: MainAxisAlignment.start,
      ),
    );
  }
}

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int numItems = context.watch<Cart>().count;
    return ClipRRect(
      child: Drawer(
        child: Container(
          color: Color.fromARGB(0xff, 0xe5, 0xe5, 0xe5),
          child: ListView(
            padding: EdgeInsets.only(top: 60, left: 45, right: 35),
            children: [
              CustomDrawerHeader(),
              Container(
                padding: EdgeInsets.only(top: 40, bottom: 11),
                child: Text(
                  'Все меню',
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromARGB(0xff, 0x33, 0x33, 0x33)
                  )
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 11, bottom: 11),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Text(
                          'Заказы',
                          style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(0xff, 0x33, 0x33, 0x33)
                          )
                        ),
                        alignment: Alignment.centerLeft
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: SizedBox(
                          width: 22,
                          child: Center(
                            child: Text(
                              '6',
                              style: TextStyle(
                                fontSize: 16,
                                color: Color.fromARGB(0xff, 0x82, 0x82, 0x82)
                              ),
                            ),
                          ),
                        ),
                        alignment: Alignment.centerRight
                      ),
                    )
                  ],
                )
              ),
              Container(
                padding: EdgeInsets.only(top: 11, bottom: 11),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        child: Text(
                          'Корзина',
                          style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(0xff, 0x33, 0x33, 0x33)
                          )
                        ),
                        alignment: Alignment.centerLeft
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: ClipOval(
                          child: Container(
                            color: Color.fromARGB(0xff, 0xf2, 0xc9, 0x4c),
                              child: SizedBox(
                                width: 22,
                                height: 22,
                                child: Center(child: Text(
                                  numItems.toString(),
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Color.fromARGB(0xff, 0x4f, 0x4f, 0x4f)
                                  )
                                )
                              )
                            )
                          ),
                        ),
                        alignment: Alignment.centerRight
                      ),
                    )
                  ],
                )
              ),
            ],
          ),
        ),
      ),
      borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
    );
  }
}
