import 'package:alt_point_food/models/cart.dart';
import 'package:alt_point_food/models/food.dart';
import 'package:alt_point_food/screens/LookupScreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MenuItem extends StatelessWidget {

  final Food _food;

  MenuItem(this._food);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: 165,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(_food.pic),
                fit: BoxFit.fitWidth
              ),
            ),
          ),
          Padding(
            child: InfoBox(_food, false),
            padding: EdgeInsets.only(left: 45, right: 45, bottom: 35, top: 20)
          )
        ],
      ),
    );
  }
}

class InfoBox extends StatelessWidget {

  final Food _food;
  final bool _isOnLookupScreen;

  InfoBox(this._food, this._isOnLookupScreen) : super();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            child: Row(
              children: [
                Container(
                  width: 185,
                  child: Column(
                    children: [
                      Container(
                        child: Text(_food.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: _isOnLookupScreen ? 24 : 14))
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        height: 20,
                        child: Row(
                          children: [
                            Container(alignment: Alignment.center, width: 14, height: 14, child: SvgPicture.asset('assets/star.svg')),
                            Container(padding: EdgeInsets.only(left: 7), child: Center(child: Text(_food.rating.toStringAsFixed(1), style: TextStyle(fontWeight: FontWeight.bold)))),
                            Container(padding: EdgeInsets.only(left: 10), child: Center(child: Text(_food.numOfReviews > 100 ? '(100+)' : '(${_food.numOfReviews})', style: TextStyle(fontWeight: FontWeight.w300)))),
                            Container(padding: EdgeInsets.only(left: 15), child: Center(child: _buildETA(_food.ETA.split(" ")))),
                          ],
                          mainAxisAlignment: MainAxisAlignment.start,
                        )
                      )
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.bottomRight,
                    height: 45,
                    child: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: '₽',
                          ),
                          TextSpan(
                            text: _food.price.toString(),
                            style: TextStyle(color: Color.fromARGB(0xff, 0x33, 0x33, 0x33), fontSize: 32, fontWeight: FontWeight.bold)
                          )
                        ]
                      )
                    ),
                  )
                )
              ],
            )
          ),
          _isOnLookupScreen ? Container(
            padding: EdgeInsets.only(top: 10, bottom: 15),
            alignment: Alignment.centerLeft,
            child: Text(_food.desc, style: TextStyle(fontWeight: FontWeight.w300)),
          ) : SizedBox(height: 20),
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: GestureDetector(
              onTap: () {
                _isOnLookupScreen ? _addItemToCart(context, _food) :
                Navigator.of(context).push(
                  PageRouteBuilder(
                    transitionDuration: Duration(milliseconds: 500),
                    pageBuilder: (_, anim, secAnim) => LookupScreen(_food),
                    transitionsBuilder: (_, anim, secAnim, child) {
                      var offsetAnimation = anim.drive(Tween(begin: Offset(1, 0), end: Offset.zero));
                      return SlideTransition(
                        child: child,
                        position: offsetAnimation,
                      );
                    }
                  )
                );
              },
              child: Container(
                height: 45,
                color: Color.fromARGB(0xff, 0xf2, 0xc9, 0x4c),
                child: Center(
                  child: Text('Купить', style: TextStyle(fontWeight: FontWeight.bold))
                )
              ),
            ),
          ),
        ],
      )
    );
  }

  void _addItemToCart(BuildContext context, Food food) {
    context.read<Cart>().add(_food);
    Scaffold.of(context).showSnackBar(SnackBar(content: Text('Заказ добавлен в корзину')));
  }

  Widget _buildETA(List<String> ETA) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: ETA[0],
            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(0xff, 0x33, 0x33, 0x33))
          ),
          TextSpan(
            text: ' '+ETA[1],
            style: TextStyle(color: Color.fromARGB(0xff, 0x33, 0x33, 0x33))
          )
        ]
      ),
    );
  }
}